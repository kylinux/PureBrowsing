# PureBrowsing
Content Filtering Script for Ubuntu users

-------------------------------------------
WHAT IT DOES
-------------------------------------------
This script will:

1) Edit resolvconf to make OpenDNS FamilyShield servers permanent on your system to block many tasteless websites.

	WARNING - This OpenDNS feature can be undone through a few steps, but that information will not be provided here because the people you are blocking this content from may use that information to circumvent the barricade. If you really must disable this filter, try Googling for a solution or just refrain from using this script altogether.

2) Download and install Pluckeye from pluckeye.net to filter images/video at the browser level on Firefox.

	a) This option can be forced to span across all browsers and programs.
	b) Pluckeye can be uninstalled via the Pluckeye Firefox plugin.
	c) See pluckeye.net for more details.

3) Add a large list of most adult sites and ads to be blocked in your local hosts file.

--------------------------------------------
PURPOSE
--------------------------------------------
This script is designed to help parents and/or individuals looking to filter questionable content from their children or themselves on Linux. It combines multiple content filtering resources into one script in order to make the process easier for Linux users running any of the various *buntu flavors.

This was tested on Ubuntu 16.04LTS and should work with versions 12.04LTS and up.

***UPDATE***

I have added a list of distracting websites to the purebrowsing.sh script. If that is too restrictive, please remove that section from the script before running the script.

------------------------------------------------
HOW TO RUN SCRIPT ON UBUNTU 12.04LTS+
------------------------------------------------

1) Download the script (purebrowsing.sh) if you haven't already.

2) Open up terminal

3) Type the following

	sudo bash purebrowsing.sh


4) Hit Enter and follow the on screen prompts

5) Restart your PC

6) Profit

-----------------------------------------------
ADDITIONAL INFORMATION
-----------------------------------------------

Visit pluckeye.net for more information on Pluckeye. Pluckeye is owned by Jon Wilkes.

Visit opendns.com for more information on OpenDNS. OpenDNS is owned by Cisco.
